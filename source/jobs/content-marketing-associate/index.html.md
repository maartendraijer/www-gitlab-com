---
layout: job_page
title: "Content Marketing Associate"
---

GitLab is looking for a highly creative, self-motivated content marketer to help drive awareness and interest in GitLab. The Content Marketing Associate position is responsible for understanding and engaging our target audiences across a variety of channels and content formats and owning content creation from start to finish.

A successful Content Marketing Associate has strong empathy and respect for the developer experience. They are able to put themselves in the shoes of others in order to understand their needs, pain points, preferences, and ambitions and translate that into multimedia experiences. Additionally, they are excited about experimentation and analyzing the results.

## Responsibilities
- Project manage content development from start to finish
- Produce and update high-quality and engaging content including (but not limited to) blog posts, webinars, emails, white papers, web pages, and case studies
- Work with product marketing to develop a deep understanding of our product messaging and translate them into engaging narratives and multimedia experiences
- Work with internal subject matter experts and thought leaders to amplify their reach and influence
- Assist in building formalized content operations processes

## Requirements
- Excellent writer and researcher with a strong ability to grasp new concepts quickly
- Degree in marketing, journalism, communications or related field
- Strong communication skills without a fear of over communication. This role will require effective collaboration and coordination across internal and external stakeholders
- Ability to empathize with the needs and experiences of developers
- Extremely detail-oriented and organized, able to meet deadlines
- You share our [values](https://about.gitlab.com/handbook/values/), and work in accordance with those values
- BONUS: A passion and strong understanding of the industry and our mission

## Hiring Process
Applicants for this position can expect the hiring process to follow the order below. Please keep in mind that applicants can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](https://about.gitlab.com/team/).

- Qualified applicants will be invited to schedule a [screening call](https://about.gitlab.com/handbook/hiring/interviewing/#screening-call) with one of our Global Recruiters.
- Candidates will complete a take-home exercise writing a 300-500 word blog post from a provided prompt. Send the completed exercise to our Content Marketing Manager.
- Next, candidates will be invited to schedule a series of 45 minute interviews with our Content Marketing Manager, Content Editor, and Senior Product Marketing Manager
- Candidates will then be invited to schedule 45 minute interviews with our Senior Director of Marketing and Sales Development and our CMO.
- Finally, our CEO may choose to conduct a final interview.
- Successful candidates will subsequently be made an offer via email.

Additional details about our process can be found on our [hiring page](https://about.gitlab.com/handbook/hiring/).
